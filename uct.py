#!/usr/bin/python3

import socket
import select
import threading

#http://www.tutorialspoint.com/python3/python_multithreading.htm
class myThread (threading.Thread):
	def __init__(self, threadID, name, sockfd):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.sockfd = sockfd
		self.running = [1]
	def run(self):
		poller(self.sockfd, self.running)

#Derived from primm, used to create a session
class status:
	def __init__(self):
		self.channel = None
		self.sock = None
		self.worker = None

#Function found at https://pymotw.com/2/select/
def poller(sockfd, running):
	READ_ONLY = select.POLLIN | select.POLLPRI | select.POLLHUP | select.POLLERR
	TIMEOUT = 200
	poller = select.poll()
	poller.register(sockfd, READ_ONLY)
	fd_to_socket = { sockfd.fileno(): sockfd,}
	while running[0]:
	# Wait for at least one of the sockets to be ready for processing
		events = poller.poll(TIMEOUT)
		for fd, flag in events:
		# Retrieve the actual socket from its file descriptor
			s = fd_to_socket[fd]
			if flag & (select.POLLIN | select.POLLPRI):
				PONG(sockfd)

#Allows user to send a message
def send_msg(session, message):
	if message:
		session.sock.send(bytes("privmsg " + message + "\n", "utf-8"))
		print(">> ", end="")
	else:
		session.sock.send(bytes("privmsg\n", "utf-8"))

#Creates a new user with username reed
def new_user(sock):
	user = "user a b c d\n"
	nick = "nick reed\n"
	sock.send(bytes(user, 'utf-8'))
	sock.send(bytes(nick, 'utf-8'))
	x = sock.recv(1024)
	print(x.decode('utf-8'))

#ping response found with the help of SPC Primm
def PONG(sock):
	x = sock.recv(1024)
	x = x.decode("utf-8")
	if "PING" in x and ":Supports" not in x:
		sock.send(bytes("PONG " + x[6:] + '\n', 'utf-8'))
	else:
		print("\n" + x + "\n>> ", end="")

#Function that will set the user as away
def away(session, message):
	if message: #sets user as away
		portions = message.split(" ", 1)
		if len(portions) > 1:
			portions[1] = ":" + portions[1]
			message = portions[0] + " " + portions[1]
		session.sock.send(bytes("away " + message + "\n", "utf-8"))
	else: # if message is None, sets user as back
		session.sock.send(bytes("away\n", "utf-8"))

#Funcition that will allow the user to leave the channel
def part(session, message):
	if "#" != message[0]:
		message = "#" + message
	portions = message.split(" ", 1)
	if len(portions) > 1:
		portions[1] = ":" + portions[1]
		message = portions[0] + " " + portions[1]

	session.sock.send(bytes("part " + message + "\n", "utf-8"))

#Function that will allow the user to send out a message with the wallops command to everyone
def wallops(session, message):
	if message:
		if message[0] != ":":
			message = ":" + message
		session.sock.send(bytes("wallops " + message + "\n", "utf-8"))
	else:
		print(">> ", end = "")

#Function that allows the user to change channels
def join(session, channel):
	if channel[0] != "#":
		channel = "#" + channel
	if session.channel:
		part(session, session.channel)
	session.channel = channel
	session.sock.send(bytes("join " + channel + "\n", "utf-8"))

#Function that allows the user to see who is on
def who(session, channel):
	if channel:
		if channel[0] != "#":
			channel = "#" + channel
		session.sock.send(bytes("who " + channel + "\n", "utf-8"))
	else:
		print(">> ", end = "")

#Function that lets the user see the help information
def send_help(session, void):
	session.sock.send(bytes("help\n", "utf-8"))

#Function that allows the user to change the nickname
def change_name(session, new_name):
	if new_name:
		if len(new_name) > 0 and len(new_name) <= 9:
			session.sock.send(bytes("nick " + new_name + "\n", "utf-8"))
		else:
			print("The length of the new username must be less than 9 characters long.\n")
	else:
		print(">> ", end = "")

#Function that allows the user to quit
def send_quit(session, void):
	session.worker.running[0] = 0
	session.worker.join()
	session.sock.close()
	exit(0)

#Function that allows the user to check who is a specific nickname
def who_is(session, name):
	if name:
		if len(name) > 0 and len(name) <= 9:
			session.sock.send(bytes("whois " + name + "\n", "utf-8"))
		else:
			print("The length of the new username must be less than 9 characters long.\n")
	else:
		print(">> ", end = "")

#Function that allows the user to ping a target
def ping(session, target):
	if target:
		if len(target) > 0 and len(target) <= 9:
			session.sock.send(bytes("ping " + target + "\n", "utf-8"))
		else:
			print("The length of the target must be less than 9 characters long.\n")
	else:
		print("You must construct aditional pylons\n")
		print(">> ", end = "")

#Function
def motd(session, void):
	session.sock.send(bytes("motd\n", "utf-8"))

#Function that lists out all the channels
def listo(session, channel):
	if channel:
		if channel[0] != "#":
			channel = "#" + channel
		session.sock.send(bytes("list " + channel + "\n", "utf-8"))
	else:
		session.sock.send(bytes("list\n", "utf-8"))

#Function that lists out all the useres on a channel
def lusers(session, void):
	session.sock.send(bytes("lusers\n", "utf-8"))

#Function that allows the user to find out the topic of a channel or change the topic
def topic(session, channel):
	if channel:	
		if channel[0] != "#":
			channel = "#" + channel
	channel = channel.split(" ", 1)

	if len(channel) > 1:
		channel[1] = ":" + channel[1]
		combined = channel[0] + " " + channel[1]
	else:
		combined = channel[0]
	
	session.sock.send(bytes("topic " + combined + "\n", "utf-8"))

#Function that allows the user to check the server to see if a nickname is currentlty on the server
def ison(session, name):
	if name:
		session.sock.send(bytes("ison " + name + "\n", "utf-8"))
	else:
		print(">> ", end="")

#Function that allows the user to use the mode command
def mode(session, command):
	if command:
		session.sock.send(bytes("mode " + command + "\n", "utf-8"))
	else:
		print(">> ", end="")

#Main function that uses a dictionary to go to the function corresponding to user input
def main():
	commands = {
	"AWAY":away, "ISON":ison, "HELP":send_help, "INFO":send_help,
	"JOIN":join, "LIST":listo, "LUSERS":lusers, "MODE":mode, "MOTD":motd, "NICK":change_name, 
	"NOTICE":send_msg, "PART":part, "PING":ping, "PRIVMSG":send_msg, "MSG":send_msg, "QUIT":send_quit, 
	"TOPIC":topic, "WALLOPS":wallops, "WHO":who, "WHOIS":who_is}

	#Creating the socket and session, then connecting with the use of the new_user function
	sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sockfd.connect(("127.0.0.1", 6667))
	session = status()
	session.sock = sockfd
	new_user(sockfd)
	session.worker = myThread(1, "Thread 1", sockfd)
	session.worker.start()
	channel = None

	print(">> ", end = "")
	#The main loop that uses a try except to catch control c, derived from primm
	while True:
		try:
			x = input()
			#allows newline character to be ignored if its by itself
			if len(x) == 0:
				print(">> ", end = "")
			#checks for a / or \\ and then splits the input to get the command and the rest of the input
			elif x[0] == '/' or x[0] == '\\':
				c = x.split(" ", 1)
				c[0] = c[0][1:].upper()
				command = c[0]
				if(len(c) > 1):
					if command in commands.keys():
						commands[command](session, c[1])
				elif command not in commands.keys():
					print(">> ", end = "")
				else:
					if command in commands.keys():
						commands[command](session, None)
			elif session.channel:
				commands["NOTICE"](session, (session.channel + " " + x))
			else:
				print(">> ", end = "")
		except KeyboardInterrupt:
			session.worker.running[0] = 0
			session.worker.join()
			session.sock.close()
			break

if __name__ == "__main__":
	main()
